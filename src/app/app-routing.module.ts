import {APP_BASE_HREF} from '@angular/common';


import { AppComponent } from './app.component'

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BaseComponent, TrunkComponent } from './car/index';
import { CarComponent } from './car/car.component';
import { DoorComponent } from './car/door/door.component';
import { EngineComponent } from './car/engine/engine.component';
import { InfoComponent } from './car/info/info.component';
import { ShareComponent } from './car/share/share.component';


const routes: Routes = [
    {
        path: '',
        redirectTo: '/car/base',
        pathMatch: 'full'
    },
    {
        path: 'car',
        component: CarComponent,
        children: [
            {
                path: 'base',
                component: BaseComponent
            },
            {
                path: 'trunk',
                component: TrunkComponent
            },
            {
                path: 'engine',
                component: EngineComponent
            },
            {
                path: 'door',
                component: DoorComponent
            },
            {
                path: 'info',
                component: InfoComponent
            },
            {
                path: 'share',
                component: ShareComponent
            }

        ]
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: true})],
    exports: [RouterModule]
})
export class AppRoutingModule { }
