import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../provider/communication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-engine',
  templateUrl: './engine.component.html',
  styleUrls: ['./engine.component.css']
})
export class EngineComponent implements OnInit {

  constructor(public communicationService:CommunicationService, public router:Router) { }
  lock:boolean;
  text_button = "Start Engine";
  engine_running: boolean = false;
  show_spinner: boolean = false;

  ngOnInit() {
    this.communicationService.sendMessage("engine");
  }

  backToBasic(){
    this.communicationService.sendMessage("base");
    this.router.navigate(['car/base']);
  }

  showInfo():void{
    this.communicationService.sendMessage("info");
  }

  engineAction():void{
    this.show_spinner = true;
    if(this.engine_running == false){
      setTimeout(()=>{  
        this.show_spinner = false;
        this.text_button = "Stop Engine";
        this.engine_running = true;
      }, 2000);
    }
    else{
      setTimeout(()=>{  
        this.show_spinner = false;
        this.text_button = "Start Engine";
        this.engine_running = false;
      }, 2000);
    }
  }  

}
