import {
  Component,
  ElementRef,
  ViewChild,
  Renderer2,
  ChangeDetectorRef
} from '@angular/core';
import {
  CommunicationService
} from '../provider/communication.service';

import { Router } from '@angular/router';
import { log } from 'util';

declare var require: any;
declare var $: any

var Hammer = require('hammerjs');

@Component({
  selector: 'car-root',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})

export class CarComponent {
  @ViewChild('car') car: ElementRef;
  public info:boolean = false;
  public blocked: boolean = false;
  private message: string;

  constructor(
    public communicationService: CommunicationService,
    private renderer: Renderer2,
    public router: Router,
    private ref: ChangeDetectorRef) {}

  ngOnInit() {
    //this.checkOperation(window.location.href.split("/")[5]);
    this.communicationService.instruction_sub_comb.subscribe(message => {
      this.checkOperation(message);
    });
    // Get a reference to an element
    var square = this.car.nativeElement;
    var manager = new Hammer.Manager(square);
    var Swipe = new Hammer.Swipe();

    manager.add(Swipe);
    let localThis = this;
    manager.on('swipe', (e) => {
      this.checkSwipeAction(e);
    });


  }

  checkOperation(message){    
    this.message = message;
    switch (message) {
      case "info":
        this.info = true;
        this.blocked = true;
        this.renderer.addClass(this.car.nativeElement, 'car-image-info');       
        this.router.navigate(['car/info']);
        break;
      case "trunk":
        this.blocked = true;
        this.renderer.addClass(this.car.nativeElement, 'car-image-trunk');
        break;
      case "engine":
        this.blocked = true;
        this.renderer.addClass(this.car.nativeElement, 'car-image-engine');
        break;
      case "door":
        this.blocked = true;
        this.renderer.addClass(this.car.nativeElement, 'car-image-door');
        break;
      case "share":
        this.blocked = true;
        this.renderer.addClass(this.car.nativeElement, 'car-image-share');
        this.router.navigate(['car/share']); 
        break;
      case "base":
        this.info = false;
        this.blocked = false;
        this.renderer.removeClass(this.car.nativeElement, 'car-image-info');
        this.renderer.removeClass(this.car.nativeElement, 'car-image-trunk');
        this.renderer.removeClass(this.car.nativeElement, 'car-image-engine');
        this.renderer.removeClass(this.car.nativeElement, 'car-image-door');
        this.renderer.removeClass(this.car.nativeElement, 'car-image-share');
        this.ref.detectChanges();
        break;
    }
  }

  checkSwipeAction(event): void{
    let direction = event.offsetDirection;  
      
    if(!this.blocked){
      if(direction == 4){
        this.checkOperation("door");
        this.router.navigate(['car/door']); 
      }
      if(direction == 8){
        this.checkOperation("trunk");
        this.router.navigate(['car/trunk']); 
      }
      if(direction == 16){
        this.checkOperation("engine");
        this.router.navigate(['car/engine']); 
      }
    }
    else{
      this.checkOperation("base");
      this.router.navigate(['car/base']); 
    }
  }

  checkCarClick(event):void{      
    console.log(event);
    if(!this.blocked){
      this.blocked = true;
      if(event.clientY <= 135){
        this.communicationService.sendMessage("engine");   
        this.router.navigate(['car/engine']);
      }
      if((event.clientY <= 360) && (event.clientY > 135)){
        this.communicationService.sendMessage("door");
        this.router.navigate(['car/door']);  
      }
      if(event.clientY > 360){
        this.communicationService.sendMessage("trunk");
        this.router.navigate(['car/trunk']);
      }
    }
  }

  getCarWidth(){
    let styles;
    styles = {
      'width': "210px",
    };

    try {  
      if(window.screen.height <730){
        styles = {
          'width': "240px",
        };
      }
      if(window.screen.height <650){
        styles = {
          'width': "210px",
        };
      }
      if(window.screen.height <625){
        styles = {
          'width': "210px"
        };
      }
    } catch (error) {
        console.log(error);
    }    
    return styles;
  }

  getWindowHeight(){
    return window.screen.height;
  }

  getWindowWidth(){
    return window.screen.width;
  }


}
