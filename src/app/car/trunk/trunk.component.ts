import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../provider/communication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'trunk',
  templateUrl: './trunk.component.html',
  styleUrls: ['./trunk.component.css']
})
export class TrunkComponent implements OnInit{
  title = 'trunk';
  lock:boolean;

  constructor(public communicationService:CommunicationService,
    public router:Router){}

  ngOnInit(){
    this.communicationService.sendMessage("trunk");
  }

  backToBasic(){
    this.communicationService.sendMessage("back");
    this.router.navigate(['car/base']);
  }

  showInfo():void{
    this.communicationService.sendMessage("info");
  }
}
