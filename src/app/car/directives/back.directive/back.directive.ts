import {Component, OnInit, OnDestroy} from '@angular/core';
import { CommunicationService } from '../../../provider/communication.service';
import { Router } from '@angular/router';


@Component({
  moduleId: module.id,
  selector: 'back', 
  styleUrls: ['./back.directive.css'],
  templateUrl: 'back.directive.html'
})

export class BackComponent {

  constructor(public communicationService:CommunicationService,
    public router:Router) { }
  
    showInfo():void{
      this.communicationService.sendMessage("info");
    }

    share(){
      this.communicationService.sendMessage("share");
    }

    checkWindowSize(){
      let retValue = false;
      if(window.screen.width < 375){
        retValue =  true;
      }
      return retValue;
    }

}