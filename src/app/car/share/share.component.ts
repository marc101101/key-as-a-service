import {
  Component,
  OnInit
} from '@angular/core';
import {
  CommunicationService
} from '../../provider/communication.service';
import {
  Router
} from '@angular/router';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.css']
})
export class ShareComponent implements OnInit {

  constructor(private communicationService: CommunicationService, private router: Router) {}

  public friendsList = [{
      "name": "UPS",
      "subtitle": "United Parcel Service",
      "image": "assets/img/ups.png",
      "key_state": true
    },
    {
      "name": "TÜV Süd",
      "subtitle": "Technical Service",
      "image": "assets/img/tuev.png",
      "key_state": false
    },
    {
      "name": "Jack Nuber",
      "subtitle": "Father",
      "image": "assets/img/father.png",
      "key_state": true
    },
    {
      "name": "Jasmin Haber",
      "subtitle": "Girlfriend",
      "image": "assets/img/friend.png",
      "key_state": true
    }
  ]
  ngOnInit() {
    this.communicationService.sendMessage("share");
  }

  backToBasic() {
    this.communicationService.sendMessage("base");
    this.router.navigate(['car/base']);
  }

}
