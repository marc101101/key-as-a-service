import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../provider/communication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-door',
  templateUrl: './door.component.html',
  styleUrls: ['./door.component.css']
})
export class DoorComponent implements OnInit {

  constructor(public communicationService:CommunicationService,
  public router:Router) { }

  lock:boolean;

  ngOnInit() {
    this.communicationService.sendMessage("door");
  }

  backToBasic(){   
    this.communicationService.sendMessage("base");
    this.router.navigate(['car/base']);
  }

  showInfo():void{
    this.communicationService.sendMessage("info");
  }

}
