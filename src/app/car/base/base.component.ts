import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../provider/communication.service';

@Component({
  selector: 'app-base',
  templateUrl: './base.component.html',
  styleUrls: ['./base.component.css']
})
export class BaseComponent implements OnInit {
  public change: boolean = false;
  lock:boolean;
  
  constructor(public communicationService:CommunicationService) { }

  ngOnInit() {
    this.communicationService.sendMessage("base");
  }

  showInfo():void{
    this.communicationService.sendMessage("info");
    this.change = true;
  }

  backToBasic(){
    this.communicationService.sendMessage("base");
    this.change = false;
  }

  checkWindowSize(){
    let retValue = false;
    if(window.screen.width < 375){
      retValue =  true;
    }
    return retValue;
  }

}
