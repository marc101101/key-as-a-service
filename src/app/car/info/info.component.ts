import { Component, OnInit } from '@angular/core';
import { CommunicationService } from '../../provider/communication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor(public communicationService:CommunicationService,
  public router:Router) { }
  public info:boolean = false;
  private blocked: boolean = false;
  lock:boolean;
  
  public info_data = [
    {
      "name": "Battery Status",
      "status": "Normal - 76%",
      "icon": "battery_charging_full"
    },
    {
      "name": "Coolant",
      "status": "Normal",
      "icon": "ac_unit"
    },
    {
      "name": "Brake Fluid",
      "status": "Low",
      "icon": "spa"
    },
    {
      "name": "Washer Fluid",
      "status": "Normal",
      "icon": "opacity"
    }
  ];

  ngOnInit() {
    this.communicationService.sendMessage("info");
  }

  backToBasic(){
    this.communicationService.sendMessage("base");
    this.router.navigate(['car/base']);
  }

}
