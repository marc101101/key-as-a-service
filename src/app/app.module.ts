import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';


import { AppComponent } from './app.component';
import { TrunkComponent } from './car/index';
import { CarComponent } from './car/car.component';
import { BaseComponent } from './car/base/base.component';
import { CommunicationService } from './provider/communication.service';
import { EngineComponent } from './car/engine/engine.component';
import { DoorComponent } from './car/door/door.component';
import { InfoComponent } from './car/info/info.component';
import { BackComponent } from './car/directives/back.directive/back.directive';
import { ShareComponent } from './car/share/share.component';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
export class MyHammerConfig extends HammerGestureConfig  {
  overrides = <any>{
    'swipe': {velocity: 0.4, threshold: 20} // override default settings
  }
}
@NgModule({
  declarations: [
    AppComponent,
    TrunkComponent,
    CarComponent,
    BaseComponent,
    EngineComponent,
    DoorComponent,
    InfoComponent,
    BackComponent,
    ShareComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    CommunicationService, 
    { 
      provide: HAMMER_GESTURE_CONFIG, 
      useClass: MyHammerConfig 
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
